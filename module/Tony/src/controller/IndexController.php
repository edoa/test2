<?php
/**
 * Created by PhpStorm.
 * User: Mr Tonny
 * Date: 03/02/2019
 * Time: 19:17
 */

namespace Tony\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }
}